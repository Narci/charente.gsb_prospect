<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit77748f5d1dbe2645dea82939db1b73b3
{
    public static $prefixLengthsPsr4 = array (
        'g' => 
        array (
            'gsb_prospects\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'gsb_prospects\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit77748f5d1dbe2645dea82939db1b73b3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit77748f5d1dbe2645dea82939db1b73b3::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
